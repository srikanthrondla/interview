package com.sts.tennis.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.sts.tennis.domain.UserDetail;
import com.sts.tennis.exception.RegistrationException;
import com.sts.tennis.validators.PasswordMatchValidator;

@RunWith(MockitoJUnitRunner.class)
public class UserValidationTest {

    @Autowired
    private Validator validator;

    private UserDetail userInfo;
    
    private PasswordMatchValidator passwordMatchValidator;
    
    private ConstraintValidatorContext constraintValidatorContext;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        passwordMatchValidator = new PasswordMatchValidator();

        userInfo = new UserDetail();
        String email = "John@gmail.com";
        userInfo.setUserName(email);
        userInfo.setFirstName("John");
        userInfo.setLastName("Smith");
        userInfo.setMiddleName("R");
        userInfo.setUserPassword("123456");
        userInfo.setConfirmPassword("123456");
    }
    @Test
    public void testInvalidFirstName() {

        UserDetail userInfoLocal = new UserDetail();
        userInfoLocal = userInfo;
        userInfoLocal.setFirstName("");
        Set<ConstraintViolation<UserDetail>> violations = validator.validate(userInfoLocal);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void testInvalidLastName() {

        UserDetail userInfoLocal = new UserDetail();
        userInfoLocal = userInfo;
        userInfoLocal.setLastName("");
        Set<ConstraintViolation<UserDetail>> violations = validator.validate(userInfoLocal);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void testInvalidEmail() {

        UserDetail userInfoLocal = new UserDetail();
        userInfoLocal = userInfo;
        userInfoLocal.setUserName("wewe");
        Set<ConstraintViolation<UserDetail>> violations = validator.validate(userInfoLocal);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void testInvalidPassword() {

        UserDetail userInfoLocal = new UserDetail();
        userInfoLocal = userInfo;
        userInfoLocal.setUserPassword("wewe");
        Set<ConstraintViolation<UserDetail>> violations = validator.validate(userInfoLocal);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void testInvalidConfirmPassword() {

        UserDetail userInfoLocal = new UserDetail();
        userInfoLocal = userInfo;
        userInfoLocal.setConfirmPassword("wewe");
        Set<ConstraintViolation<UserDetail>> violations = validator.validate(userInfoLocal);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void testValidUserDetail() {
        Set<ConstraintViolation<UserDetail>> violations = validator.validate(userInfo);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void testPasswordValidator() {
        Set<ConstraintViolation<UserDetail>> violations = validator.validate(userInfo);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void testNullConfirmPassword() {
        UserDetail userInfoLocal = new UserDetail();
         userInfoLocal = userInfo;
        userInfoLocal.setConfirmPassword(null);
        Set<ConstraintViolation<UserDetail>> violations = validator.validate(userInfoLocal);
        assertFalse(violations.isEmpty());
    }
    
    @Test
    public void testNullPassword() {
        UserDetail userInfoLocal = new UserDetail();
        userInfoLocal = userInfo;
        userInfoLocal.setUserPassword(null);
        Set<ConstraintViolation<UserDetail>> violations = validator.validate(userInfoLocal);
        assertFalse(violations.isEmpty());
    }
    
    /**
     * Password Match validations below
     */
    @Test(expected=NullPointerException.class)
    public void testPasswordMatchValidatorNull(){
        passwordMatchValidator.initialize(null);
    }
    
    @Test(expected=RegistrationException.class)
    public void testPasswordMatchValidatorIsValidNull(){
        passwordMatchValidator.isValid(null, constraintValidatorContext);
    }
    
    @Test
    public void testPasswordMatchValidatorIsValid(){
        passwordMatchValidator.setFirstFieldName("userPassword");
        passwordMatchValidator.setSecondFieldName("confirmPassword");
        boolean result =   passwordMatchValidator.isValid(userInfo, constraintValidatorContext);
        assertTrue(result);
    }
    
    @Test
    public void testPasswordMatchValidatorIsValidNullFirst(){
        UserDetail userInfoLocal = userInfo;
        passwordMatchValidator.setFirstFieldName("userPassword");
        passwordMatchValidator.setSecondFieldName("confirmPassword");
        userInfoLocal.setUserPassword(null);
        boolean result =  passwordMatchValidator.isValid(userInfoLocal, constraintValidatorContext);
        assertFalse(result);
    }
    
    @Test
    public void testPasswordMatchValidatorIsValidNullLast(){
        UserDetail userInfoLocal = userInfo;
        passwordMatchValidator.setFirstFieldName("userPassword");
        passwordMatchValidator.setSecondFieldName("confirmPassword");
        userInfoLocal.setConfirmPassword(null);
       boolean result = passwordMatchValidator.isValid(userInfoLocal, constraintValidatorContext);
       assertFalse(result);
    }
    @Test
    public void testPasswordMatchValidatorIsValidDiff(){
        UserDetail userInfoLocal = userInfo;
        passwordMatchValidator.setFirstFieldName("userPassword");
        passwordMatchValidator.setSecondFieldName("confirmPassword");
        userInfoLocal.setConfirmPassword("1234567");
        boolean result = passwordMatchValidator.isValid(userInfoLocal, constraintValidatorContext);
        
        assertFalse(result);
    }
    
    @Test(expected=RegistrationException.class)
    public void testPasswordMatchValidatorIsValidException(){
       passwordMatchValidator.isValid(userInfo, constraintValidatorContext);
    }

}
