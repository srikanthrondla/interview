package com.sts.tennis.service;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.sts.tennis.Application;
import com.sts.tennis.domain.UserDetail;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application.class)
public class RegistrationServiceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationServiceTest.class);
	@Autowired
	private RegistrationService registrationService;
	
	
	private UserDetail userInfo;
	
	@Before
	public void setUp() throws Exception {
		userInfo = new UserDetail();
		String email = "John"+Calendar.getInstance().getTimeInMillis() +"@gmail.com";
		userInfo.setUserName(email);
		userInfo.setFirstName("John");
		userInfo.setLastName("Smith");
		userInfo.setMiddleName("R");
		userInfo.setUserPassword("123456");
		userInfo.setConfirmPassword("123456");
				
	}
	
	/**
	 * Test availability interval results at various times of the day. 
	 */
	@Test
	public void testCreateRegistration() {
	    LOGGER.debug("Test create registration");
		 UserDetail output = registrationService.savePlayerDetails(userInfo);
		
	}
	
	
	
	
	
	

	
}
