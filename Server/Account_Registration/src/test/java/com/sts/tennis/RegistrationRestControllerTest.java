package com.sts.tennis;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Calendar;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.sts.tennis.domain.UserDetail;

/**
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application.class)
public class RegistrationRestControllerTest  {

	private static final Logger log = LoggerFactory.getLogger(RegistrationRestControllerTest.class);
	

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
    

    private MockMvc mockMvc;
    

    @Autowired
    private HttpMessageConverter mappingJackson2HttpMessageConverter;


    @Autowired
    private WebApplicationContext webApplicationContext;


    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
  }


    @Test
    public void createEventPostRequestTest() throws Exception {
        UserDetail userInfo = new UserDetail();
    	userInfo.setFirstName("Test name");
    	userInfo.setLastName("Test last Name");
    	userInfo.setUserName("test"+Calendar.getInstance().getTimeInMillis() +"@gmail.com");
    	userInfo.setUserPassword("password");
    	userInfo.setConfirmPassword("password");
    	  String eventJson = json(userInfo);
    	  RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
  				"/create").contentType(contentType)
                  .content(eventJson);
        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse() .getContentAsString());
		
		String expected = "{userId:0,createdBy:null,createdDate:null,firstName:"+userInfo.getFirstName()+",lastName:"+userInfo.getLastName()+",middleName:null,upadatedDate:null,updatedBy:null,userName:"+userInfo.getUserName()+",userPassword:"+userInfo.getUserPassword()+",confirmPassword:"+userInfo.getConfirmPassword()+"}";

		//"{response:Success,responseCode:200}";

		//JSONAssert.assertEquals(expected, result.getResponse()
		//		.getContentAsString(), false);
        
    }
    
    
    @Test
    public void loginEventPostRequestTest() throws Exception {
        /*UserDetail userInfo = new UserDetail();
        userInfo.setFirstName("Test name");
        userInfo.setLastName("Test last Name");
        userInfo.setUserName("aarush109@gmail.com");
        userInfo.setUserPassword("j123456");
        userInfo.setConfirmPassword("j123456");
          String eventJson = json(userInfo);*/
          RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/login").contentType(contentType).param("id", "aarush109@gmail.com").param("password", "j123456");
                  //.content(eventJson);
        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse() .getContentAsString());
        String expected = "{response:Success,responseCode:200}";


        //JSONAssert.assertEquals(expected, result.getResponse()
         //       .getContentAsString(), false);
        
    }
    @Test
    public void loginEventPostRequestFailureTest() throws Exception {
      //  UserDetail userInfo = new UserDetail();
      //  userInfo.setUserName("test11@gmail.com");
      //    String eventJson = json(userInfo);
          RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/login").contentType(contentType).param("id", "test11@gmail.com");
                 // .content(eventJson);
        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();
        JSONAssert.assertEquals("400",  Integer.toString(result.getResponse().getStatus()), false);
        
    }
    
    
    
    @Test
    public void createEventPostRequestFailureTest() throws Exception {
    	UserDetail userInfo = new UserDetail();
    	userInfo.setLastName("Test last Name");
    	userInfo.setUserName("test11@gmail.com");
    	userInfo.setUserPassword("password");
    	userInfo.setConfirmPassword("password1");
    	  String eventJson = json(userInfo);
    	  RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
  				"/create").contentType(contentType)
                  .content(eventJson);
        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();
		JSONAssert.assertEquals("400",  Integer.toString(result.getResponse().getStatus()), false);
        
    }

    protected String json(Object obj) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                obj, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        log.info(mockHttpOutputMessage.getBodyAsString());
        return mockHttpOutputMessage.getBodyAsString();
    }
}