package com.sts.tennis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sts.tennis.domain.UserDetail;

@Repository
public interface UserRepository   extends CrudRepository<UserDetail, Long>{
    
    public UserDetail findByUserName(String userName);

}
