package com.sts.tennis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sts.tennis.domain.UserDetail;
import com.sts.tennis.repository.UserRepository;

@Service
public class RegistrationService {

    @Autowired
    private UserRepository userRepository;
    
    public UserDetail getPlayerDetails(UserDetail player){
        UserDetail response =  userRepository.findByUserName(player.getUserName());
        if(response !=null && player.getUserPassword().equals(response.getUserPassword())){
            return response;
        }else{
            return null;
        }
    }
    
    public UserDetail savePlayerDetails(UserDetail player){
      return userRepository.save(player);
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    

}
