package com.sts.tennis;

import java.io.IOException;

import javax.naming.ServiceUnavailableException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sts.tennis.domain.UserDetail;
import com.sts.tennis.service.RegistrationService;
/**
 * 
 * User operations
 * 
 * @author Srikanth
 *
 */
@CrossOrigin(origins = "*", allowedHeaders = "*") // TO disable CORS policy
@RestController
public class RegistrationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    RegistrationService registrationService;

    /**
     * Create registration using input
     * @param userInfoJson
     * @return CreateResponse
     */
    @GetMapping(value = {"/login"})
    public UserDetail login(@RequestParam(name = "id")  String userName,@RequestParam(name = "password")  String password) {
        UserDetail userInfo = new UserDetail();
        userInfo.setUserName(userName);
        userInfo.setUserPassword(password);
          return  registrationService.getPlayerDetails(userInfo);

    }

    /**
     * Login user(Mapping from ui is not done)
     * 
     * @param userInfo
     * @return
     */
    @PostMapping(value = {"/create"})
    public UserDetail create(@Valid @RequestBody UserDetail userInfo) {
       
        LOGGER.debug("Login "); 
           return registrationService.savePlayerDetails(userInfo);
    }

    @ExceptionHandler({ServiceUnavailableException.class})
    public void handleServiceUnavailable(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.FORBIDDEN.value(), "The request is not within the time period that the service is available");
    }
}
