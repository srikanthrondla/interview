package com.sts.tennis.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sts.tennis.validators.PasswordMatch;


/**
 * The persistent class for the user_details database table.
 * 
 */
@PasswordMatch(first = "userPassword", second = "confirmPassword", message = "Passwords must match")
@Entity
@Table(name="user_details")
@NamedQuery(name="UserDetail.findAll", query="SELECT u FROM UserDetail u")
public class UserDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_id")
	private int userId;

	@Column(name="created_by")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="created_date")
	private Date createdDate;

	@NotNull(message = "First Name cannot be null")
    @Size(min = 1, max = 255, message = "First name must be between 1 and 255 characters")
	@Column(name="first_name")
	private String firstName;

	@NotNull(message = "Last Name cannot be null")
    @Size(min = 1, max = 255, message = "Last name must be between 1 and 255 characters")
	@Column(name="last_name")
	private String lastName;

	@Column(name="middle_name")
	private String middleName;

	@Temporal(TemporalType.DATE)
	@Column(name="upadated_date")
	private Date upadatedDate;

	@Column(name="updated_by")
	private String updatedBy;

	@NotNull(message = "Email  cannot be null")
    @Email(message = "Email should be valid")
	@Column(name="user_name")
	private String userName;

	@NotNull(message = "Password  cannot be null")
    @Size(min = 6, message = "Password should be min 6 characters")
	@Column(name="user_password")
	private String userPassword;
	
	@NotNull(message = "Confirm Password  cannot be null")
    @Size(min = 6, message = "Password should be min 6 characters")
	@Transient
    private String confirmPassword;

	public UserDetail() {
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getUpadatedDate() {
		return this.upadatedDate;
	}

	public void setUpadatedDate(Date upadatedDate) {
		this.upadatedDate = upadatedDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

}