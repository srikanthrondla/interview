package com.sts.tennis.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sts.tennis.exception.RegistrationException;

/**
 * 
 *
 */
public class PasswordMatchValidator implements ConstraintValidator<PasswordMatch, Object> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PasswordMatchValidator.class);
    private String firstFieldName;
    private String secondFieldName;
    @Override
    public void initialize(PasswordMatch constraintAnnotation) {
        firstFieldName = constraintAnnotation.first();
        secondFieldName = constraintAnnotation.second();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext cxt) {
        try {
            final Object firstObj = BeanUtils.getProperty(value, firstFieldName);
            final Object secondObj = BeanUtils.getProperty(value, secondFieldName);

            return firstObj == null && secondObj == null || firstObj != null && firstObj.equals(secondObj);
        } catch (final Exception ignore) {
            LOGGER.error("Error in validator " + ignore);
            throw new RegistrationException(ignore.getMessage());
        }
    }

    public String getFirstFieldName() {
        return firstFieldName;
    }

    public void setFirstFieldName(String firstFieldName) {
        this.firstFieldName = firstFieldName;
    }

    public String getSecondFieldName() {
        return secondFieldName;
    }

    public void setSecondFieldName(String secondFieldName) {
        this.secondFieldName = secondFieldName;
    }

}
