import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  createURL: string = "http://localhost:8080/create";
  loginApiURL: string = "http://localhost:8080/login";

  constructor(private httpClient: HttpClient) {}

  public createRegistration(form) {
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    };
    return this.httpClient.post(this.createURL, form, httpOptions);
  }

  public loginUser(form) {
    let reqUrl =
      this.loginApiURL + "?id=" + form.email + "&password=" + form.password;
    return this.httpClient.get(reqUrl);
  }
}
