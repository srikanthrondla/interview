import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../../api.service";

import { Router } from "@angular/router";
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
  FormGroupDirective,
  NgForm,
  ValidatorFn
} from "@angular/forms";

@Component({
  selector: "app-log-in",
  templateUrl: "./log-in.component.html",
  styleUrls: ["./log-in.component.css"]
})
export class LogInComponent implements OnInit {
  private errorMessage: any;
  private responseMessage;
  private sub: any;
  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    public formBuilder: FormBuilder,
    private router: Router
  ) {}
  loginForm: FormGroup;

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.responseMessage = params["id"];
      if ("Success" == this.responseMessage) {
        this.responseMessage = "Registration sucessfull,Please login.";
      }
    });

    this.reactiveForm();
  }

  /* Reactive form */
  reactiveForm() {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.maxLength(255)]],
      password: ["", Validators.required]
    });
  }

  submitForm() {
    this.apiService.loginUser(this.loginForm.value).subscribe((data: any[]) => {
      //console.log(data);

      if (data != null) {
        this.router.navigate(["/dashboard"], { state: { data } });
      } else {
        this.errorMessage = "Invalid Username/Password";
      }
      /*const userStr = JSON.stringify(res.body);
   
   JSON.parse(userStr, (key, value) => {
  if (key === 'responseCode' && value === 200) {
    this.errorMessage = "Login Success";
    
  }else{
  if( this.errorMessage != "Login Success")
   this.errorMessage = "Login Failied"
   
  }*/
    });
  }
}
