import { Component, OnInit } from "@angular/core";
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
  FormGroupDirective,
  NgForm,
  ValidatorFn
} from "@angular/forms";
import { errorMessages } from "./error-messages";
import { ErrorStateMatcher } from "@angular/material";
import { HttpResponse } from "@angular/common/http";

import { ApiService } from "../../api.service";
import { Router } from "@angular/router";

/** Error when the parent is invalid */
class CrossFieldErrorMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    return control.dirty && form.invalid;
  }
}

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  private errorMessage: any;
  registrationForm: FormGroup;
  errorMatcher = new CrossFieldErrorMatcher();

  errors = errorMessages;

  constructor(
    public formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.reactiveForm();
  }

  /* Reactive form */
  reactiveForm() {
    this.registrationForm = this.formBuilder.group(
      {
        firstName: ["", [Validators.required, Validators.maxLength(255)]],
        userName: ["", [Validators.required, Validators.email]],
        lastName: ["", [Validators.required, Validators.maxLength(255)]],
        middleName: [""],

        userPassword: ["", Validators.required],
        confirmPassword: ["", Validators.required]
      },
      { validator: this.passwordValidator }
    );
  }

  /**
Create registration using user inputs
**/
  createRegistrationForm() {
    this.apiService
      .createRegistration(this.registrationForm.value)
      .subscribe((data: any[]) => {
        //console.log(data);

        if (data != null) {
          this.router.navigate(["/dashboard"], { state: { data } });
        } else {
          this.errorMessage = "Account not registered";
        }
      });
  }

  passwordValidator(form: FormGroup) {
    const condition =
      form.get("userPassword").value !== form.get("confirmPassword").value;

    return condition ? { passwordsDoNotMatch: true } : null;
  }
}
