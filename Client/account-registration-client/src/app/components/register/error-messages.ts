/**
 * Collection of reusable error messages
 */
export const errorMessages: { [key: string]: string } = {
  fistName: 'First name is required',
  lastName: 'Last name is required',
  email: 'Email must be a valid email address (username@domain)',
  password: 'Please enter 6 or more characters without leading or trailing spaces.',
  confirmPassword: 'Passwords must match'
};