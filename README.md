

# Angular UI

##	Assumption Node js and npm should be available

 1)	I have used below versions

 2)	 Node –v  v12.11.0

 3)	npm –v 6.11.3

 ## Pick account-registration-client from client folder

 1)	npm install

 2) Got to project directory in terminal and run
 
    ng serve 

 3)	Should be accessible with  [http://localhost:4200]

# Databse

 1)Execute DB_Design.sql in my sql

# Spring boot application

1)	Assumption run in Eclipse Ide with spring boot plugin or have STS Ide

2)	Pick Account_Registration from server

3)	Import →Existing project in workspace

4) Change database connection properties in application-<name>.yml file

4)	In the vm arguments make sure -Dspring.profiles.active=srikanth is set.

 

If different profile need to be used create new application-<name>.yml file in project config directory
And run with -Dspring.profiles.active=<name>

4)	Run as spring boot 

6) Should be accessible from 


[http://localhost:8080]


7)	API spec can be pulled from. Once the application is up


[http://localhost:8080/swagger-ui.html]



 


# App testing

##	If the angular ui and spring boot app is setup can test using the ui
 
 1)	From UI fill in the registration
 
 2)	All required fields are marked with *
 
3)	Password and confirm password should match
 
 4)	Password should be minimum of 6 
 
 5)	If successful registration will be redirected to login screen
 
 6)	Upon Failure Registration failed message will be displayed


 


 

 


