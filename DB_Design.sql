CREATE TABLE user_details
  (
    
    user_id      INT NOT NULL,
    first_name    VARCHAR(100)  NOT NULL,
    last_name     VARCHAR(100)  NOT NULL,
    middle_name   VARCHAR(100),
    user_name      VARCHAR(100)   NOT NULL,
    user_password   VARCHAR(100)   NOT NULL,
    upadated_date DATE,
    updated_by    VARCHAR(50),
    created_date  DATE,
    created_by    VARCHAR(50),
    CONSTRAINT user_pk PRIMARY KEY (user_id),
     CONSTRAINT unique_constraint_name UNIQUE(user_name)
    
    
  );
